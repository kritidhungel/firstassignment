package LinearAutomation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CerotidWebsiteLinearExample {
	
	static WebDriver driver;
	private static String state;
	
	public static void main(String[] args) {
		
		// Step 1: invoking the Browser with the Method
				invokeBrowser();
				
		// fill form with the method
		fillform();
		// Select Course
		//Creating Webelement Obj to find the location of the element
		WebElement course = driver.findElement(By.xpath("//select[@id= 'classType' ]"));
		//creating select obj and passing the element
	    Select chooseCourse = new Select(course);
	    //creating a string variable with course name
		String courseQAAutomation = "QA Automation";
		//selecting the course by visible text
		chooseCourse.selectByVisibleText(courseQAAutomation);
		
		//Selection session date
		WebElement session = driver.findElement(By.xpath("//select[@id='sessionType']"));
		Select chooseSession = new Select(session);
		String sessionType = "Upcoming Session";
		chooseSession.selectByVisibleText(sessionType);
		
		//selecting how did you hear about us
		WebElement msource = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		Select chooseMsource = new Select(msource);
		String mediasource = "Friends/Family";
		chooseMsource.selectByVisibleText(mediasource);
		
		// tying full name
		WebElement fullname = driver.findElement(By.xpath("//input[@id='name']"));
		fullname.sendKeys("Kriti Dhungel");
				
		//typing address
		WebElement address = driver.findElement(By.xpath("//input[@id='address']"));
		address.sendKeys("1076 Wyandott Cicrle");
		
		//typing city
		WebElement city = driver.findElement(By.xpath("//input[@id='city']"));
		city.sendKeys("Thornton");
				
				
		//selecting state
		WebElement state = driver.findElement(By.xpath("//select[@id='state']"));
		Select chooseState= new Select(state);
		String State = "CO";
		chooseState.selectByVisibleText(State);
				
		//typing zipcode
		WebElement zipcode = driver.findElement(By.xpath("//input[@id='zip']"));
		zipcode.sendKeys("80260");
		
		//typing email
		WebElement email = driver.findElement(By.xpath("//input[@id='email']"));
		email.sendKeys("kritidhungel13@gmail.com");
				
		//tying phone number
		WebElement phone = driver.findElement(By.xpath("//input[@id='phone']"));
		phone.sendKeys("720-266-0839");
				
		//typing educationbackground
		WebElement educationbackground = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		educationbackground.sendKeys("Bachelors Degree");
		
		
		
		
		
		
		
		
		
		
	}

	private static void fillform() {
		// 
		
	}

	private static void invokeBrowser() {
		// Setting the system path
		System.setProperty("webdriver.chrome.driver", "./lib/chromedriver");
		
		// creating new chromedriver obj
		driver = new ChromeDriver();

		// navigating to cerotid website
		driver.navigate().to("http://www.cerotid.com");
		
		System.out.println(driver.getTitle() + "-----------------------------------------Was launched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

}
